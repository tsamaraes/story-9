from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import index, like, dislike, top5Books
from .models import Books
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Story9Test(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
		response = Client().get('/like-book')
		self.assertEqual(response.status_code, 301)
		response = Client().get('/dislike-book')
		self.assertEqual(response.status_code, 301)
		response = Client().get('/top-5-books')
		self.assertEqual(response.status_code, 301)

	def test_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')
	
	def test_using_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)
		found = resolve('/like-book/')
		self.assertEqual(found.func, like)
		found = resolve('/dislike-book/')
		self.assertEqual(found.func, dislike)
		found = resolve('/top-5-books/')
		self.assertEqual(found.func, top5Books)

	def test_books_model(self):
		Books.objects.create(
			book_id = 123,
    		image_link = '',
    		title = "Sarah's Book",
    		authors = '',
    		publisher = '',
    		like_count = 0
		)
		my_book = Books.objects.get(title="Sarah's Book")
		self.assertEqual(str(my_book), "Sarah's Book")

class Story9FunctionalTest(LiveServerTestCase):
	def setUp(self):
		# super().setUp()
		# chrome_options = webdriver.ChromeOptions()
		# self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story9FunctionalTest,self).setUp()

	def tearDown(self):
		self.driver.quit()
		super().tearDown()
		# super(Story9FunctionalTest,self).tearDown()

	def test_search_for_books_then_like_some_books_then_see_modal(self):
		self.driver.get(self.live_server_url)
		response_page = self.driver.page_source
		
		keyword = self.driver.find_element_by_id('searchField')
		keyword.send_keys('Harry Potter')
		time.sleep(2)

		title = self.driver.find_elements_by_id('title')
		self.assertIn('Harry Potter', title[1].text)

		like_button = self.driver.find_elements_by_id('like-button')
		dislike_button = self.driver.find_elements_by_id('dislike-button')
		like_button[1].click()
		like_button[1].click()
		time.sleep(2)

		like_button[2].click()
		like_button[2].click()
		dislike_button[2].click()
		time.sleep(2)

		button = self.driver.find_element_by_id('top5')
		button.click()
		time.sleep(5)

		like_modal = self.driver.find_elements_by_id('like-count')
		self.assertIn('2', like_modal[0].text)

		like_modal = self.driver.find_elements_by_id('like-count')
		self.assertIn('1', like_modal[1].text)

	def test_search_for_books_with_undefined_author(self):
		self.driver.get(self.live_server_url)
		response_page = self.driver.page_source
		
		ipa = 'IPA Terpadu'
		for i in ipa:
			self.driver.find_element_by_id('searchField').send_keys(i)
			time.sleep(0.1)
			
		time.sleep(5)

		title = self.driver.find_elements_by_id('title')
		author = self.driver.find_elements_by_id('author')
		self.assertIn('undefined', author[1].text)

		dislike_button = self.driver.find_elements_by_id('dislike-button')
		dislike_button[1].click()
		like_count = self.driver.find_elements_by_id('like-counter')
		self.assertIn('0', like_count[1].text)
