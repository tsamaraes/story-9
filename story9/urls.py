from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('like-book/', views.like, name='like'),
    path('dislike-book/', views.dislike, name='dislike'),
    path('top-5-books/', views.top5Books, name='top5Books'),
]